<h1 align="center">客户关系管理系统｜ CRM</h1>



![image-20210114110841401](https://gitee.com/yacongliu/images/raw/master/img/20210114110841.png)

​		CRM 系统即客户关系管理系统， 顾名思义就是管理公司与客户之间的关系。 是一种以"客户关系一对一理论"为基础，旨在改善企业与客户之间关系的新型管理机制。客户关系管理的定义是：企业为提高核心竞争力，利用相应的信息技术以及互联网技术来协调企业与顾客间在销售、营销和服务上的交互，从而提升其管理方式，向客户提供创新式的个性化的客户交互和服务的过程。 其最终目标是吸引新客户以及将已有客户转化为忠实客户，增加公司市场份额。

​		CRM 的实施目标就是通过全面提升企业业务流程的管理来降低企业成本，通过提供更快速和周到的优质服务来吸引和保持更多的客户。作为一种新型管理机制，CRM 极大地改善了企业与客户之间的关系，应用于企业的市场营销、销售、服务与技术支持等与客户相关的领域。



## 1. 技术栈



![image-20210114111747592](https://gitee.com/yacongliu/images/raw/master/img/20210114111747.png)

## 1.1 构建项目

​		构建项目先设计好全局流程，例如自定义异常配合全局异常处理器进行异常的统一处理响应给前端进行友好性提示及引导、定义响应给前端的JSON格式等。

### 1.1.1 工具类

**基础类**

<img src="https://gitee.com/yacongliu/images/raw/master/img/20210114123253.png" alt="image-20210114123253125" style="zoom:80%;" />

`BaseController` 用于Controller中设置上下文及提供常用方法。

`BaseMapper` 用于Dao层中提供常用数据库操作方法。

`BaseService` 结合BaseMapper用户表的常规CURD操作。

`BaseQuery` 用于列表条件查询。

`ResultInfo` 定义响应给前端的统一数据格式。



**异常类**

<img src="https://gitee.com/yacongliu/images/raw/master/img/20210114123856.png" alt="image-20210114123856927" style="zoom:80%;" />

`NoLoginException` 定义未登录异常，配合拦截器及全局异常处理类实现非法请求拦截。

`ParamsException` 自定义参数异常，配合AssertUtil.isTrue()及全局异常处理类实现参数校验等响应前端。

**工具类**

<img src="https://gitee.com/yacongliu/images/raw/master/img/20210114123957.png" alt="image-20210114123957277" style="zoom:80%;" />



**全局异常处理类**

<img src="https://gitee.com/yacongliu/images/raw/master/img/20210114124814.png" alt="image-20210114124021705" style="zoom:80%;" />



### 1.1.2 Maven逆向工程

​		逆向工程简单理解就是先设计数据库表，再通过maven逆向技术自动生成java文件的过程。

​		使用resources目录下的genertorConfig.xml，其中根据需要修改数据库驱动位置，表明，生成的文件位置。

​		Edit Configurations 配置并maven命令：mybatis-generator:generate -e  运行成功后，就会在项目中看到自动生成的实体类，Mapper接口，Mapper文件。

![image-20210114122401979](https://gitee.com/yacongliu/images/raw/master/img/20210114122402.png)


## 2. 功能模块



### 2.1 用户管理

![image-20210115172945316](https://gitee.com/yacongliu/images/raw/master/img/20210115172945.png)



#### 2.1.1 用户登录

![image-20210114114253845](https://gitee.com/yacongliu/images/raw/master/img/20210114114253.png)

**整体思路：**

>1. 将登录表单中的用户名，密码通过Ajax传输给后端
>2. 后端进行入参校验，判断用户记录是否存在以及将前端传输的密码进行加密后与数据库中存储的密码进行对比，判断密码是否一致。
>3. 登录成功后返回前端需要的登录用户信息，前端将接受到的用户信息存储Cookie中，并判断登录时是否勾选记住密码，7天免登录，进而设定Cookie有效期。



**核心代码：**

```java
@Service
public class UserService extends BaseService<User, Integer> {

    @Autowired
    private UserMapper userMapper;

    /**
     * @param userName 用户名
     * @param passWord 密码
     * @return void
     * @description 用户登录
     * @date 2021/1/13 16:19
     **/
    public UserVo userLogin(String userName, String passWord) {
        // 1. 参数校验
        checkLoginParams(userName, passWord);
        // 2. 通过用户名查询用户记录
        User user = userMapper.queryUserByName(userName);
        // 3. 判断用户对象是否为空
        AssertUtil.isTrue(null == user, "用户不存在！");
        // 4. 判断密码是否一致。比对客户端传输的密码与数据库中存储的密码是否一致
        checkPassWord(passWord, user.getUserPwd());
        // 5. 用户登录成功，返回用户相关信息
        return buildUserInfo(user);
    }
}

		/**
     * @param userName 用户名
     * @param passWord 密码
     * @return void
     * @description 参数校验
     * @date 2021/1/13 16:23
     **/
    private void checkLoginParams(String userName, String passWord) {
        AssertUtil.isTrue(StringUtils.isBlank(userName), "用户名不能为空！");
        AssertUtil.isTrue(StringUtils.isBlank(passWord), "密码不能为空！");
    }

		/**
     * @param user            用户对象
     * @param newPassWord     新密码
     * @param oldPassWord     旧密码
     * @param confirmPassWord 确认密码
     * @return void
     * @description 验证修改密码参数
     * 用户ID：userId 非空 用户对象必须存在
     * 原始密码：oldPassword 非空 与数据库中密文密码保持一致
     * 新密码：newPassword 非空 与原始密码不能相同
     * 确认密码：confirmPassword 非空 与新密码保持一致
     * @date 2021/1/13 22:02
     **/
    private void checkUpdatePassWordParams(User user, String newPassWord, String oldPassWord, String confirmPassWord) {

        AssertUtil.isTrue(null == user, "用户未登录或不存在！");
        AssertUtil.isTrue(StringUtils.isBlank(newPassWord), "请输入新密码！");
        AssertUtil.isTrue(StringUtils.isBlank(oldPassWord), "请输入原始密码！");
        AssertUtil.isTrue(!(user.getUserPwd().equals(Md5Util.encode(oldPassWord))), "原始密码不正确！");
        AssertUtil.isTrue(oldPassWord.equals(newPassWord), "新密码不能与原始密码相同！");
        AssertUtil.isTrue(StringUtils.isBlank(confirmPassWord), "请输入确认密码！");
        AssertUtil.isTrue(!(newPassWord.equals(confirmPassWord)), "新密码与确认密码不一致！");
    }

		/**
     * @param user 用户记录
     * @return UserVo
     * @description 构建需要返回给前台的用户信息
     * @date 2021/1/13 16:34
     **/
    private UserVo buildUserInfo(User user) {
        UserVo userVo = new UserVo();
        //userVo.setUserId(user.getId());
        userVo.setUserIdStr(UserIDBase64.encoderUserID(user.getId())); //对用户ID进行加密
        userVo.setUserName(user.getUserName());
        userVo.setTrueName(user.getTrueName());
        return userVo;
    }
```



```javascript
layui.use(['form', 'jquery', 'jquery_cookie'], function () {
    var form = layui.form,
        layer = layui.layer,
        $ = layui.jquery,
        $ = layui.jquery_cookie($);
    /**
     * 用户登录
     */
    form.on("submit(login)", function (res) {
        // 获取表单元素的值
        var fieldData = res.field;

        // 判断参数是否为空
        if (fieldData.username === "undefined" || fieldData.username.trim() === "") {
            layer.msg("用户名不能为空！");
            return false;
        }

        if (fieldData.password === "undefined" || fieldData.password === "") {
            layer.msg("密码不能为空！");
            return false;
        }

        $.ajax({
            type: "post",
            url: ctx + "/user/login",
            data: {
                userName: fieldData.username,
                passWord: fieldData.password
            },
            dataType: "json",
            success: function (data) {
                if (data.code === 200) {
                    layer.msg("登录成功！", function () {
                        // 将用户信息存到cookie中
                        var result = data.result;
                        $.cookie("userIdStr", result.userIdStr);
                        $.cookie("userName", result.userName);
                        $.cookie("trueName", result.trueName);

                        //是否勾选记住密码，7天免登录
                        if ($(".rememberMe").is(":checked")) {
                            console.log("勾选了记住密码");
                            $.cookie("userIdStr", result.userIdStr, {
                                expires: 7
                            });
                            $.cookie("userName", result.userName, {
                                expires: 7
                            });
                            $.cookie("trueName", result.trueName, {
                                expires: 7
                            });
                        }
                        // 登录成功后，跳转到首页
                        window.location.href = ctx + "/main";
                    });
                } else {
                    layer.msg(data.msg);
                }
            }
        });
        //阻止表单跳转
        return false;
    });

});
```



#### 2.1.2 用户退出

<img src="https://gitee.com/yacongliu/images/raw/master/img/20210114115833.png" alt="image-20210114115833734" style="zoom:80%;" />

整体思路：

>删除Cookie并跳转至登录页面

```html
<a href="javascript:;" class="login-out">退出登录</a>
```

```javascript
$(".login-out").click(function () {
    //删除cookie
    $.removeCookie("userIdStr", {domain: window.location.hostname, path: ctx});
    $.removeCookie("userName", {domain: window.location.hostname, path: ctx});
    $.removeCookie("trueName", {domain: window.location.hostname, path: ctx});
    //跳转到登录页面
    window.location.href = ctx + "/index";
});
```



#### 2.1.3 密码修改

![image-20210114120426901](https://gitee.com/yacongliu/images/raw/master/img/20210114120426.png)

**整体思路：**

> 1. 从HttpServletRequest request中获取Cookie中用户ID
> 2. 入参校验：非空校验，用户是否真实存在校验，原始密码是否正确，新密码与原始密码不能相同，新密码与确认密码是否一致
> 3. 对新密码进行加密并执行密码更新 (事务控制)
> 4. 前端清空Cookie，跳转至登录页面



**核心代码：**

```java
>>>Controller
@PostMapping("user/updatePassword")
@ResponseBody
public ResultInfo updatePassWord(HttpServletRequest request, String newPassWord, String oldPassWord, String confirmPassWord) {

        ResultInfo resultInfo = new ResultInfo();
        // 1. Cookie中获取userId
        int userId = LoginUserUtil.releaseUserIdFromCookie(request);
        // 2. 修改密码
        userService.updateUserPassWord(userId, newPassWord, oldPassWord, confirmPassWord);
        return resultInfo;
 }

>>>Service
 @Transactional(propagation = Propagation.REQUIRED)
 public void updateUserPassWord(Integer userId, String newPassWord, String oldPassWord, String confirmPassWord) {

        // 1. 通过userId获取用户对象
        User user = userMapper.selectByPrimaryKey(userId);
        // 2. 参数校验
        checkUpdatePassWordParams(user, newPassWord, oldPassWord, confirmPassWord);
        // 3. 设置用户新密码 新密码进行加密
        user.setUserPwd(Md5Util.encode(newPassWord));
        // 4. 执行更新操作 受影响的行数小于1，更新失败
        AssertUtil.isTrue(userMapper.updateByPrimaryKeySelective(user) < 1, "密码更新失败！");

    }

    /**
     * @param user            用户对象
     * @param newPassWord     新密码
     * @param oldPassWord     旧密码
     * @param confirmPassWord 确认密码
     * @return void
     * @description 验证修改密码参数
     * 用户ID：userId 非空 用户对象必须存在
     * 原始密码：oldPassword 非空 与数据库中密文密码保持一致
     * 新密码：newPassword 非空 与原始密码不能相同
     * 确认密码：confirmPassword 非空 与新密码保持一致
     * @date 2021/1/13 22:02
     **/
    private void checkUpdatePassWordParams(User user, String newPassWord, String oldPassWord, String confirmPassWord) {

        AssertUtil.isTrue(null == user, "用户未登录或不存在！");
        AssertUtil.isTrue(StringUtils.isBlank(newPassWord), "请输入新密码！");
        AssertUtil.isTrue(StringUtils.isBlank(oldPassWord), "请输入原始密码！");
        AssertUtil.isTrue(!(user.getUserPwd().equals(Md5Util.encode(oldPassWord))), "原始密码不正确！");
        AssertUtil.isTrue(oldPassWord.equals(newPassWord), "新密码不能与原始密码相同！");
        AssertUtil.isTrue(StringUtils.isBlank(confirmPassWord), "请输入确认密码！");
        AssertUtil.isTrue(!(newPassWord.equals(confirmPassWord)), "新密码与确认密码不一致！");
    }
```



```javascript
layui.use(['form', 'jquery', 'jquery_cookie'], function () {
    var form = layui.form,
        layer = layui.layer,
        $ = layui.jquery;
    $ = layui.jquery_cookie($);

    form.on("submit(saveBtn)", function (res) {
        // 获取表单元素的值
        var fieldData = res.field;

        $.ajax({
            type: "post",
            url: ctx + "/user/updatePassword",
            data: {
                newPassWord: fieldData.new_password,
                oldPassWord: fieldData.old_password,
                confirmPassWord: fieldData.again_password
            },
            dataType: "json",
            success: function (data) {
                if (data.code === 200) {
                    layer.msg("用户密码修改成功，系统将在3秒钟后退出...", function () {
                        // 退出系统后，删除对应的cookie
                        $.removeCookie("userIdStr", {domain: window.location.hostname, path: ctx});
                        $.removeCookie("userName", {domain: window.location.hostname, path: ctx});
                        $.removeCookie("trueName", {domain: window.location.hostname, path: ctx});
                        // 跳转到登录页面 (父窗口跳转)
                        window.parent.location.href = ctx + "/index";
                    });
                } else {
                    layer.msg(data.msg);
                }
            }
        });
        //阻止表单跳转
        return false;
    });

});
```



### 2.2 权限管理









### 2.3 营销管理



#### 2.3.1 机会管理

![image-20210115173731078](https://gitee.com/yacongliu/images/raw/master/img/20210115173731.png)



##### 2.3.1.1 机会查询

![image-20210115183315309](https://gitee.com/yacongliu/images/raw/master/img/20210115183315.png)

**整体思路：**

> 1. 创建用于多条件查询的对象集成BaseQuery
> 2. 进行分页查询
> 3. 按照前端列表数据格式响应数据



**核心代码：**

```java
>>>Controller
  
    @GetMapping("list")
    @ResponseBody
    public Map<String, Object> querySaleChanceByParams(SaleChanceQuery query) {
        return saleChanceService.querySaleChanceByParams(query);
    }

>>>Service
  
    /**
     * @param query 营销机会查询对象
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @description 多条件分页查询
     * @date 2021/1/14 14:49
     **/
    public Map<String, Object> querySaleChanceByParams(SaleChanceQuery query) {

        PageHelper.startPage(query.getPage(), query.getLimit());
        PageInfo<SaleChance> pageInfo = new PageInfo<>(saleChanceMapper.selectByParams(query));
        return buildResult(pageInfo);
    }

>>>Mapper
  
  <!--u.user_name as uname 是为了将用户ID转换成具体的值，并在该SaleChance对象中增加 uname属性 响应给前台 -->
  
      <select id="selectByParams" parameterType="com.mayi.crm.query.SaleChanceQuery"
            resultType="com.mayi.crm.model.SaleChance">
        select
        s.*,u.user_name as uname
        from t_sale_chance s
        left join t_user u on s.assign_man = u.id
        where s.is_valid = 1
        <if test="customerName != null and customerName != ''">
            and s.customer_name like concat('%',#{customerName},'%')
        </if>
        <if test="createMan != null and createMan != ''">
            and s.create_man = #{createMan}
        </if>
        <if test="state != null and state != ''">
            and s.state = #{state}
        </if>
    </select>
```



```javascript
layui.use(['table', 'layer'], function () {
    var layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table;


    /**
     * 渲染表格
     */
    var tableIns = table.render({
        elem: '#saleChanceList', // 表格绑定的ID
        url: ctx + '/sale_chance/list', // 访问数据的地址
        cellMinWidth: 95,
        page: true, // 开启分页
        height: "full-125",
        limits: [10, 15, 20, 25],
        limit: 10,
        toolbar: "#toolbarDemo",
        id: "saleChanceListTable",
        cols: [[
            {type: "checkbox", fixed: "center"},
            {field: "id", title: '编号', fixed: "true", sort: true},
            {field: 'chanceSource', title: '机会来源', align: "center"},
            {field: 'customerName', title: '客户名称', align: 'center'},
            {field: 'cgjl', title: '成功几率', align: 'center'},
            {field: 'overview', title: '概要', align: 'center'},
            {field: 'linkMan', title: '联系人', align: 'center'},
            {field: 'linkPhone', title: '联系电话', align: 'center'},
            {field: 'description', title: '描述', align: 'center'},
            {field: 'createMan', title: '创建人', align: 'center'},
            {field: 'createDate', title: '创建时间', align: 'center', sort: true},
            {field: 'uname', title: '指派人', align: 'center'},
            {field: 'assignTime', title: '分配时间', align: 'center', sort: true},
            {
                field: 'state', title: '分配状态',
                align: 'center', templet: function (d) {
                    return formatState(d.state);
                }
            },
            {
                field: 'devResult', title: '开发状态',
                align: 'center', templet: function (d) {
                    return formatDevResult(d.devResult);
                }
            },
            {
                title: '操作',
                templet: '#saleChanceListBar', fixed: "right", align: "center", minWidth: 150
            }
        ]]
    });
  
  	// 查询	
    $(".search_btn").click(function () {
        table.reload('saleChanceListTable', {
            where: { //设定异步数据接口的额外参数，任意设
                customerName: $("input[name='customerName']").val(), // 客户名
                createMan: $("input[name='createMan']").val(), // 创建人
                state: $("#state").val() // 状态
            }
            , page: {
                curr: 1 // 重新从第 1 页开始
            }
        }); // 只重载数据
    });


    /**
     *  格式化分配状态
     *   0：未分配
     *   1：已分配
     *   其他：未知
     * @param state
     * @returns {string}
     */
    function formatState(state) {
        if (state === 0) {
            return "<div style='color: yellowgreen'>未分配</div>";
        } else if (state === 1) {
            return "<div style='color: green'>已分配</div>";
        } else {
            return "<div style='color: red'>未知</div>"
        }
    }

    /**
     * 格式化开发状态
     * 0 - 未开发
     * 1 - 开发中
     * 2 - 开发成功
     * 3 - 开发失败
     * @param val
     * @returns {string}
     */
    function formatDevResult(val) {
        if (val === 0) {
            return "<div style='color: yellowgreen'>未开发</div>";
        } else if (val === 1) {
            return "<div style='color: #00FF00;'>开发中</div>";
        } else if (val === 2) {
            return "<div style='color: #00B83F'>开发成功</div>";
        } else if (val === 3) {
            return "<div style='color: red'>开发失败</div>";
        } else {
            return "<div style='color: #af0000'>未知</div>"
        }
    }
});

```



##### 2.3.1.2 机会添加 ｜ 更新

<img src="https://gitee.com/yacongliu/images/raw/master/img/20210115183357.png" alt="image-20210115183357668" style="zoom: 33%;" /><img src="https://gitee.com/yacongliu/images/raw/master/img/20210115183452.png" alt="image-20210115183452585" style="zoom: 33%;" />

**整体思路：**

机会添加和更新共用一个界面，通过表单中隐藏域ID来进行区分新增/更新。

> 1. 监听表格头工具栏中的按钮 **添加** 行工具栏中的按钮**编辑**
> 2. 根据表单中的隐藏域ID是否有值来区分 **新增｜更新** 操作
> 3. 执行对应业务层逻辑



**核心代码：**

```java
>>>Controller
  
    @RequestMapping("save")
    @ResponseBody
    public ResultInfo saveSaleChance(HttpServletRequest request, SaleChance saleChance) {
        // 1. 从Cookie中获取当前用户并设置创建人
        String currentUser = CookieUtil.getCookieValue(request, "userName");
        saleChance.setCreateMan(currentUser);
        // 2. 新增营销机会
        saleChanceService.saveSaleChance(saleChance);
        return success("营销机会数据添加成功！");
    }

    @PostMapping("update")
    @ResponseBody
    public ResultInfo updateSaleChance(SaleChance saleChance) {
        saleChanceService.updateSaleChance(saleChance);
        return success("营销机会更新成功！");
    }

    /**
    * 页面跳转 新增/更新
    */
		@GetMapping("addOrUpdateSaleChancePage")
    public String addOrUpdateSaleChancePage( Integer id, Model model) {
        /*
             营销机会添加页面与新增页面共用
                    修改页面需要根据传输的ID响应对应记录数据
         */
        if (null != id) {
            SaleChance saleChance = saleChanceService.selectByPrimaryKey(id);
            model.addAttribute("saleChance", saleChance);
        }

        return "saleChance/add_update";
    }
  
  
>>>Service
  
  @Transactional(propagation = Propagation.REQUIRED)
  public void saveSaleChance(SaleChance saleChance) {
        // 1. 参数校验
        checkParams(saleChance.getCustomerName(), saleChance.getLinkMan(), saleChance.getLinkPhone());
        // 2. 设置默认值
        saleChance.setState(StateStatusEnum.UNSTATE.getType()); //未分配
        saleChance.setDevResult(DevResultEnum.UNDEV.getStatus()); //未开发
        if (StringUtils.isNotBlank(saleChance.getAssignMan())) { //选择了分配人
            saleChance.setState(StateStatusEnum.STATED.getType());
            saleChance.setDevResult(DevResultEnum.DEVING.getStatus());
            saleChance.setAssignTime(new Date());
        }
        saleChance.setIsValid(1);
        saleChance.setCreateDate(new Date());
        saleChance.setUpdateDate(new Date());

        AssertUtil.isTrue(saleChanceMapper.insertSelective(saleChance) < 1, "营销机会数据添加失败！");

    }


    /**
     * @param [saleChance] 营销机会对象
     * @return void
     * @description 更新营销机会
     * @date 2021/1/15 11:00
     **/
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateSaleChance(SaleChance saleChance) {

        // 1. 参数校验
        //  判断记录是否存在
        SaleChance temp = saleChanceMapper.selectByPrimaryKey(saleChance.getId());
        AssertUtil.isTrue(null == temp, "待更新记录不存在！");
        //  基础参数校验
        checkParams(saleChance.getCustomerName(), saleChance.getLinkMan(), saleChance.getLinkPhone());

        // 2. 设置相关参数值
        saleChance.setUpdateDate(new Date());
        if (StringUtils.isBlank(temp.getAssignMan()) && StringUtils.isNotBlank(saleChance.getAssignMan())) {
            //原始未指定分配人 更新时指定
            saleChance.setState(StateStatusEnum.STATED.getType());
            saleChance.setAssignMan(saleChance.getAssignMan());
            saleChance.setAssignTime(new Date());
            saleChance.setDevResult(DevResultEnum.DEVING.getStatus());

        } else if (StringUtils.isNotBlank(temp.getAssignMan()) && StringUtils.isBlank(saleChance.getAssignMan())) {
            // 原始已指定 更新时未指定
            saleChance.setAssignMan("");
            saleChance.setState(StateStatusEnum.UNSTATE.getType());
            saleChance.setAssignTime(null);
            saleChance.setDevResult(DevResultEnum.UNDEV.getStatus());

        }

        // 3. 执行更新 判断结果
        AssertUtil.isTrue(saleChanceMapper.updateByPrimaryKeySelective(saleChance) < 1, "营销机会数据更新失败！");
    }
  
```



**表格触发点：**

```java
    /**
     * 头部工具栏
     */
    table.on('toolbar(saleChances)', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id)
        switch (obj.event) {
            case 'add':
                //打开添加营销机会添加界面
                openAddOrUpdateSaleChanceDialog(null);
                break;
            case 'del':
                deleteSaleChance(checkStatus.data);
                break;
        }
    });

    /**
     * 行工具栏
     */
    table.on('tool(saleChances)', function (obj) {
        var data = obj.data;
        var layevent = obj.event;
        var currentRowId = data.id;
        console.log(currentRowId);

        switch (layevent) {
            case 'edit':
                openAddOrUpdateSaleChanceDialog(currentRowId);
                break
            case 'del':
                deleteOneSaleChance(data.id);
                break

        }
    });


    /**
     * 打开营销机会添加或更新界面
     */
    function openAddOrUpdateSaleChanceDialog(saleChanceId) {
        var title = "<h3>营销机会管理-添加机会</h3>";
        var url = ctx + "/sale_chance/addOrUpdateSaleChancePage";

        if (saleChanceId) { //不为空则为修改界面
            title = "<h3>营销机会管理-机会更新</h3>";
            url = url + "?id=" + saleChanceId;
        }

        layui.layer.open({
            title: title,
            type: 2,
            content: url,
            area: ["500px", "620px"],
            maxmin: true
        });
    }
```



**添加/更新表单：**

```java
layui.use(['form', 'layer'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;


    /**
     * 提交表单
     */
    form.on("submit(addOrUpdateSaleChance)", function (data) {
        var index = layer.msg("数据提交中，请稍后...", {
            icon: 16,
            time: false,//不关闭
            shade: 0.8 //遮罩层透明度
        });

        var url = ctx + "/sale_chance/save";

        if ($("input[name='id']").val()) { //ID隐藏域不为空则为更新
            url = ctx + "/sale_chance/update";
        }

        $.post(url, data.field, function (result) {
            console.log(result);
            if (result.code === 200) {
                // 1. 提示成功
                layer.msg("添加成功！");
                // 2. 关闭加载层
                layer.close(index);
                // 3. 关闭弹出层
                layer.closeAll("iframe");
                // 4.刷新父页面，重新渲染表格
                parent.location.reload();
            } else {
                layer.msg(result.msg);
            }
        });

        // 阻止表单提交
        return false;
    });

    /**
     * 关闭弹出层
     */
    $('#closeBtn').click(function () {
        // 获取当前弹出层的索引
        var index = parent.layer.getFrameIndex(window.name);
        //关闭弹出层
        parent.layer.close(index);
    });

    /**
     * 加载指派人下拉框
     */

    $.get(ctx + "/user/queryAllSales", function (data) {
        //1. 判断隐藏域指派人的值，区分是否是修改
        var assignMan = $("input[name='assignMan']").val();
        //2. 如果是修改，需要回显指派人
        for (var i = 0; i < data.length; i++) {
            // 如果修改记录的指派人与响应的值中的一直，设置下拉框该项为选中
            if (assignMan == data[i].id) {
                $("#assignMan").append('<option value="' + data[i].id + '" selected>' + data[i].uname + '</option>');

            } else {
                $("#assignMan").append('<option value="' + data[i].id + '">' + data[i].uname + '</option>');
            }
        }

        //3. 重新渲染下拉框
        layui.form.render("select");
    });

});
```



##### 2.3.1.4 机会删除

![image-20210115183531694](https://gitee.com/yacongliu/images/raw/master/img/20210115183531.png)

**整体思路：**

> 1. 批量删除和单个删除
> 2. 执行删除逻辑，更新记录数据状态



**核心代码：**

```java
>>>Controller
    
  	/**
     * @description 删除营销机会
     * @param [ids]
     * @return com.mayi.crm.base.ResultInfo
     * @date 2021/1/15 15:04
     **/
    @RequestMapping("del")
    @ResponseBody
    public ResultInfo deleteSaleChance(Integer[] ids){
        saleChanceService.deleteSaleChance(ids);
        return success("营销机会删除成功！");
    }
      

>>>Service
  
  	/**
     * @description 营销机会删除
     * @param [ids]
     * @return void
     * @date 2021/1/15 15:01
     **/
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteSaleChance(Integer[] ids) {
        // 1. 入参校验
        AssertUtil.isTrue((null == ids || ids.length < 0),"请选择需要删除的数据！");
        // 2. 执行删除 判断结果
        AssertUtil.isTrue(saleChanceMapper.deleteBatch(ids)<0,"营销机会删除失败！");
    }

>>>Mapper
  
   <!--批量删除-->
    <update id="deleteBatch">
        update t_sale_chance
        set is_valid = 0
        where
        id in
        <foreach collection="array" item="id" open="(" close=")" separator=",">
            #{id}
        </foreach>

    </update>
  
```



**表格触发点，头工具栏，行工具栏：**

```javascript
 /**
     * 头部工具栏
     */
    table.on('toolbar(saleChances)', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id)
        switch (obj.event) {
            case 'add':
                //打开添加营销机会添加界面
                openAddOrUpdateSaleChanceDialog(null);
                break;
            case 'del':
                deleteSaleChance(checkStatus.data);
                break;
        }
    });

    /**
     * 行工具栏
     */
    table.on('tool(saleChances)', function (obj) {
        var data = obj.data;
        var layevent = obj.event;
        var currentRowId = data.id;
        console.log(currentRowId);

        switch (layevent) {
            case 'edit':
                openAddOrUpdateSaleChanceDialog(currentRowId);
                break
            case 'del':
                deleteOneSaleChance(data.id);
                break

        }
    });



		/**
     * 删除营销机会数据
     * @param data
     */
    function deleteSaleChance(data) {
        if (data.length === 0) {
            layer.msg("请选择要删除的记录！");
            return;
        }

        layer.confirm("确定要删除选中的记录吗？", {btn: ["确定", "取消"]}, function (index) {
            layer.close(index); //关闭确认框
            // 数组 ids=1&ids=2&ids=3
            var ids = "ids=";
            for (var i = 0; i < data.length; i++) {
                if (i < data.length - 1) {
                    ids = ids + data[i].id + "&ids=";
                } else {
                    ids = ids + data[i].id;
                }
            }

            $.ajax({
                type: "post",
                url: ctx + "/sale_chance/del",
                data: ids, // 参数传递的是数组
                dataType: "json",
                success: function (result) {
                    if (result.code === 200) {
                        // 加载表格
                        tableIns.reload();
                    } else {
                        layer.msg(result.msg, {icon: 5});
                    }
                }
            });

        });
    }

		/**
     * 单个删除营销机会
     * @param id
     */
    function deleteOneSaleChance(id) {
        // 询问是否确认删除
        layer.confirm("确定要删除这条记录吗？", {icon: 3, title: "营销机会数据管理"},
            function (index) {
                // 关闭窗口
                layer.close(index);
                // 发送ajax请求，删除记录
                $.ajax({
                    type: "post",
                    url: ctx + "/sale_chance/del",
                    data: {
                        ids: id
                    },
                    dataType: "json",
                    success: function (result) {
                        if (result.code === 200) {
                            // 加载表格
                            tableIns.reload();
                        } else {
                            layer.msg(result.msg, {icon: 5});
                        }
                    }
                });
            });
    }
```



#### 2.3.2 开发计划管理









### 2.4 客户管理







### 2.5 服务管理







### 2.6 报表分析











## 3. 参考资料

鸣谢UP:【**兹昂**】

BiliBili视频链接:https://www.bilibili.com/video/BV1af4y1v7jR

![image-20210114112748347](https://gitee.com/yacongliu/images/raw/master/img/20210114112821.png)









