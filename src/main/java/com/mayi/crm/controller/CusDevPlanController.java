package com.mayi.crm.controller;

import com.mayi.crm.base.BaseController;
import com.mayi.crm.base.ResultInfo;
import com.mayi.crm.model.CusDevPlan;
import com.mayi.crm.model.SaleChance;
import com.mayi.crm.query.CusDevPlanQuery;
import com.mayi.crm.service.CusDevPlanService;
import com.mayi.crm.service.SaleChanceService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author yacong_liu
 * @version 1.0
 * @classname CusDevPlanController
 * @description
 * @date 2021/1/16
 **/
@Controller
@RequestMapping("cus_dev_plan")
public class CusDevPlanController extends BaseController {

    @Resource
    private SaleChanceService saleChanceService;

    @Resource
    private CusDevPlanService cusDevPlanService;


    /**
     * @param [query] 客户开发计划查询对象
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @description 多条件查询客户开发计划
     * @date 2021/1/16 11:46
     **/
    @GetMapping("list")
    @ResponseBody
    public Map<String, Object> queryCusDevPlanByParams(CusDevPlanQuery query) {
        return cusDevPlanService.queryCusDevPlanByParams(query);
    }

    /**
     * @param [cusDevPlan] 客户开发计划项
     * @return com.mayi.crm.base.ResultInfo
     * @description 保存开发计划项
     * @date 2021/1/16 11:46
     **/
    @PostMapping("save")
    @ResponseBody
    public ResultInfo saveCusDecPlan(CusDevPlan cusDevPlan) {
        cusDevPlanService.saveCusDecPlan(cusDevPlan);
        return success("计划项添加成功！");

    }

    /**
     * @param [cusDevPlan]
     * @return com.mayi.crm.base.ResultInfo
     * @description 更新客户开发计划项
     * @date 2021/1/16 13:32
     **/
    @PostMapping("update")
    @ResponseBody
    public ResultInfo updateCusDevPlan(CusDevPlan cusDevPlan) {
        cusDevPlanService.updateCusDevPlan(cusDevPlan);
        return success("计划项更新成功！");
    }

    @RequestMapping("delete")
    @ResponseBody
    public ResultInfo deleteCusDevPlan(Integer id){
        cusDevPlanService.delCusDevPlan(id);
        return success("计划项删除成功！");
    }

    /**
     * @param [sid, id, model]
     * @return java.lang.String
     * @description 跳转到计划数据项
     * @date 2021/1/16 13:44
     **/
    @GetMapping("addOrUpdateCusDevPlanPage")
    public String addOrUpdateCusDevPlanPage(Integer sid, Integer id, Model model) {
        model.addAttribute("cusDevPlan", cusDevPlanService.selectByPrimaryKey(id));
        model.addAttribute("sid", sid);
        return "cusDevPlan/add_update";
    }

    /**
     * 跳转客户开发计划列表页面
     *
     * @return
     */
    @GetMapping("index")
    public String index() {
        return "cusDevPlan/cus_dev_plan";
    }

    /**
     * 跳转客户开发计划页面
     *
     * @param sid
     * @param model
     * @return
     */
    @RequestMapping("toCusDevPlanDataPage")
    public String toCusDevPlanDataPage(Integer sid, Model model) {
        SaleChance saleChance = saleChanceService.selectByPrimaryKey(sid);
        model.addAttribute("saleChance", saleChance);
        return "cusDevPlan/cus_dev_plan_data";
    }
}
