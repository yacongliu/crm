package com.mayi.crm.controller;

import com.mayi.crm.base.BaseController;
import com.mayi.crm.model.User;
import com.mayi.crm.service.PermissionService;
import com.mayi.crm.service.UserService;
import com.mayi.crm.utils.LoginUserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author yacong_liu
 * @version 1.0
 * @classname IndexController
 * @description
 * @date 2021/1/13
 **/
@Controller
public class IndexController extends BaseController {

    @Resource
    private UserService userService;
    @Resource
    private PermissionService permissionService;

    /**
     * 系统登录
     *
     * @return
     */
    @RequestMapping("index")
    private String index() {
        return "index";
    }

    /**
     * 系统欢迎
     *
     * @return
     */
    @RequestMapping("welcome")
    private String welcome() {
        return "welcome";
    }

    /**
     * 后台管理界面
     *
     * @return
     */
    @RequestMapping("main")
    private String main(HttpServletRequest request) {

        // 1. 从Cookie中获取当前登录用户的ID
        int userId = LoginUserUtil.releaseUserIdFromCookie(request);

        // 2. 根据ID查询用户对象
        User user = userService.selectByPrimaryKey(userId);

        // 3. 将用户对象设置到session中
        request.getSession().setAttribute("user",user);

        // 4. 查询用户拥有的的资源权限码
        List<String> permissions= permissionService.queryUserHasRoleHasPermissionByUserId(userId);
        request.getSession().setAttribute("permissions",permissions);

        return "main";
    }
}
