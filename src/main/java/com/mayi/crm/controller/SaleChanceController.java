package com.mayi.crm.controller;

import com.mayi.crm.base.BaseController;
import com.mayi.crm.base.ResultInfo;
import com.mayi.crm.model.SaleChance;
import com.mayi.crm.query.SaleChanceQuery;
import com.mayi.crm.service.SaleChanceService;
import com.mayi.crm.utils.CookieUtil;
import com.mayi.crm.utils.LoginUserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author yacong_liu
 * @version 1.0
 * @classname SaleChanceController
 * @description 营销机会控制层
 * @date 2021/1/14
 **/
@Controller
@RequestMapping("sale_chance")
public class SaleChanceController extends BaseController {

    @Autowired
    private SaleChanceService saleChanceService;

    @GetMapping("list")
    @ResponseBody
    public Map<String, Object> querySaleChanceByParams(SaleChanceQuery query, HttpServletRequest request, Integer flag) {

        //增加查询条件 用于开发计划多条件查询 通过flag进行区分 flag=1则为开发计划多条件查询
        if (null != flag && flag == 1) {
            // 获取当前登录人，用户开发计划过滤分配人，只显示当前用户的分配记录
            Integer currentUserId = LoginUserUtil.releaseUserIdFromCookie(request);
            query.setAssignMan(currentUserId);
        }
        return saleChanceService.querySaleChanceByParams(query);
    }

    @RequestMapping("save")
    @ResponseBody
    public ResultInfo saveSaleChance(HttpServletRequest request, SaleChance saleChance) {
        // 1. 从Cookie中获取当前用户并设置创建人
        String currentUser = CookieUtil.getCookieValue(request, "userName");
        saleChance.setCreateMan(currentUser);
        // 2. 新增营销机会
        saleChanceService.saveSaleChance(saleChance);
        return success("营销机会数据添加成功！");
    }

    @PostMapping("update")
    @ResponseBody
    public ResultInfo updateSaleChance(SaleChance saleChance) {
        saleChanceService.updateSaleChance(saleChance);
        return success("营销机会更新成功！");
    }

    @GetMapping("addOrUpdateSaleChancePage")
    public String addOrUpdateSaleChancePage(Integer id, Model model) {
        /*
             营销机会添加页面与新增页面共用
                    修改页面需要根据传输的ID响应对应记录数据
         */
        if (null != id) {
            SaleChance saleChance = saleChanceService.selectByPrimaryKey(id);
            model.addAttribute("saleChance", saleChance);
        }

        return "saleChance/add_update";
    }

    /**
     * @param [ids]
     * @return com.mayi.crm.base.ResultInfo
     * @description 删除营销机会
     * @date 2021/1/15 15:04
     **/
    @RequestMapping("del")
    @ResponseBody
    public ResultInfo deleteSaleChance(Integer[] ids) {
        saleChanceService.deleteSaleChance(ids);
        return success("营销机会删除成功！");
    }

    @RequestMapping("updateSaleChanceDevResult")
    @ResponseBody
    public ResultInfo updateSaleChanceDevResult(Integer id, Integer devResult) {
        saleChanceService.updateSaleChanceDevResult(id, devResult);
        return success("开发状态更新成功！");
    }

    @GetMapping("index")
    public String index() {
        return "saleChance/sale_chance";
    }

}
