package com.mayi.crm.controller;

import com.mayi.crm.base.BaseController;
import com.mayi.crm.base.ResultInfo;
import com.mayi.crm.model.User;
import com.mayi.crm.query.UserQuery;
import com.mayi.crm.service.UserService;
import com.mayi.crm.utils.LoginUserUtil;
import com.mayi.crm.vo.UserVo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author yacong_liu
 * @version 1.0
 * @classname UserController
 * @description
 * @date 2021/1/13
 **/
@Controller
public class UserController extends BaseController {

    @Resource
    private UserService userService;

    /**
     * @param userName 客户端传入的用户名
     * @param passWord 客户端传入的密码
     * @return com.mayi.crm.base.ResultInfo
     * @description 用户登录
     * @date 2021/1/13 16:48
     **/
    @PostMapping("user/login")
    @ResponseBody
    public ResultInfo userLogin(String userName, String passWord) {

        ResultInfo result = new ResultInfo();
        UserVo userVo = userService.userLogin(userName, passWord);
        /**
         * 登录成功后有两种处理：
         *  1. 将用户的登录信息存入Session （问题：重启服务器，Session失效，客户端需要重新登录）
         *  2。 将用户信息返回给客户端，由客户端（Cookie）保存。采用Cookie
         */
        result.setResult(userVo);
        return result;
    }


    @PostMapping("user/updatePassword")
    @ResponseBody
    public ResultInfo updatePassWord(HttpServletRequest request, String newPassWord, String oldPassWord, String confirmPassWord) {

        ResultInfo resultInfo = new ResultInfo();
        // 1. Cookie中获取userId
        int userId = LoginUserUtil.releaseUserIdFromCookie(request);
        // 2. 修改密码
        userService.updateUserPassWord(userId, newPassWord, oldPassWord, confirmPassWord);
        return resultInfo;
    }

    /**
     * @param []
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @description 查询所有销售人员
     * @date 2021/1/15 13:40
     **/
    @GetMapping("user/queryAllSales")
    @ResponseBody
    public List<Map<String, Object>> queryAllSales() {
        return userService.queryAllSales();

    }

    @GetMapping("user/list")
    @ResponseBody
    public Map<String, Object> queryUserByParams(UserQuery query) {
        return userService.queryUserByParams(query);
    }

    @PostMapping("user/save")
    @ResponseBody
    public ResultInfo saveUser(User user) {
        userService.saveUser(user);
        return success("用户添加成功！");
    }

    @PostMapping("user/update")
    @ResponseBody
    public ResultInfo updateUser(User user) {
        userService.updateUser(user);
        return success("用户更新成功！");
    }

    @RequestMapping("user/delete")
    @ResponseBody
    public ResultInfo deleteUser(Integer[] ids) {
        userService.deleteUserByIds(ids);
        return success("用户记录删除成功！");
    }

    @GetMapping("user/toPasswordPage")
    public String toPasswordPage() {
        return "user/password";
    }

    @GetMapping("user/index")
    public String index() {
        return "user/user";
    }

    @GetMapping("user/addOrUpdateUserPage")
    public String addOrUpdateUserPage(Integer id, Model model) {
        if (null != id) {
            model.addAttribute("u", userService.selectByPrimaryKey(id));
        }
        return "user/add_update";
    }


}
