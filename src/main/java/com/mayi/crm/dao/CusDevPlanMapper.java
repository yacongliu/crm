package com.mayi.crm.dao;

import com.mayi.crm.base.BaseMapper;
import com.mayi.crm.model.CusDevPlan;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface CusDevPlanMapper extends BaseMapper<CusDevPlan, Integer> {

}