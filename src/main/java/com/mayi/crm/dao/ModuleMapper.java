package com.mayi.crm.dao;

import com.mayi.crm.base.BaseMapper;
import com.mayi.crm.model.Module;
import com.mayi.crm.vo.TreeVo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ModuleMapper extends BaseMapper<Module, Integer> {

    List<TreeVo> queryAllModules();

    List<Module> queryModules();

    Module queryModuleByGradeAndModuleName(Integer grade, String moduleName);

    Module queryModuleByGradeAndUrl(Integer grade, String url);

    // 通过权限码查询资源对象
    Module queryModuleByOptValue(String optValue);

    // 查询指定资源是否存在子记录
    Integer queryModuleByParentId(Integer id);
}