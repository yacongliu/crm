package com.mayi.crm.dao;

import com.mayi.crm.base.BaseMapper;
import com.mayi.crm.model.Role;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
@Mapper
public interface RoleMapper extends BaseMapper<Role, Integer> {

    public List<Map<String, Object>> queryAllRoles(Integer userId);

    Role selectByRoleName(String roleName);
}