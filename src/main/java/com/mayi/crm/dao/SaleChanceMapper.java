package com.mayi.crm.dao;

import com.mayi.crm.base.BaseMapper;
import com.mayi.crm.model.SaleChance;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SaleChanceMapper extends BaseMapper<SaleChance,Integer> {

}