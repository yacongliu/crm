package com.mayi.crm.dao;

import com.mayi.crm.base.BaseMapper;
import com.mayi.crm.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface UserMapper extends BaseMapper<User, Integer> {

    public User queryUserByName(String userName);

    public List<Map<String, Object>> queryAllSales();

}