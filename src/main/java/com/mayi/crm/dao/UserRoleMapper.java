package com.mayi.crm.dao;

import com.mayi.crm.base.BaseMapper;
import com.mayi.crm.model.UserRole;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole,Integer> {

    public Integer countUserRoleByUserId(Integer userId);

    public Integer deleteUserRoleByUserId(Integer userId);

}