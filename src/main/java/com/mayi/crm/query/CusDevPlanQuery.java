package com.mayi.crm.query;

import com.mayi.crm.base.BaseQuery;

/**
 * @author yacong_liu
 * @version 1.0
 * @classname CusDevPlanQuery
 * @description
 * @date 2021/1/16
 **/
public class CusDevPlanQuery extends BaseQuery {

    private Integer sid; //营销机会id

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }
}
