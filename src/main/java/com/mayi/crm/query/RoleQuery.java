package com.mayi.crm.query;

import com.mayi.crm.base.BaseQuery;

/**
 * @author yacong_liu
 * @version 1.0
 * @classname RoleQuery
 * @description 角色查询
 * @date 2021/1/23
 **/
public class RoleQuery extends BaseQuery {

    //角色名
    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
