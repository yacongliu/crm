package com.mayi.crm.query;

import com.mayi.crm.base.BaseQuery;

/**
 * @author yacong_liu
 * @version 1.0
 * @classname SaleChanceQuery
 * @description 营销机会多条件查询对象
 * @date 2021/1/14
 **/
public class SaleChanceQuery extends BaseQuery {

    /*
    营销机会查询条件
     */
    private String customerName; //客户名称
    private String createMan; //创建人
    private String state;//分配状态

    /*
    开发计划查询条件
     */
    private Integer devResult; //开发状态
    private Integer assignMan; //分配人

    public Integer getDevResult() {
        return devResult;
    }

    public void setDevResult(Integer devResult) {
        this.devResult = devResult;
    }

    public Integer getAssignMan() {
        return assignMan;
    }

    public void setAssignMan(Integer assignMan) {
        this.assignMan = assignMan;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCreateMan() {
        return createMan;
    }

    public void setCreateMan(String createMan) {
        this.createMan = createMan;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
