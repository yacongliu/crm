package com.mayi.crm.query;

import com.mayi.crm.base.BaseQuery;

/**
 * @author yacong_liu
 * @version 1.0
 * @classname UserQuery
 * @description
 * @date 2021/1/17
 **/
public class UserQuery extends BaseQuery {

    private String userName;
    private String email;
    private String phone;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
