package com.mayi.crm.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mayi.crm.base.BaseService;
import com.mayi.crm.dao.CusDevPlanMapper;
import com.mayi.crm.dao.SaleChanceMapper;
import com.mayi.crm.model.CusDevPlan;
import com.mayi.crm.query.CusDevPlanQuery;
import com.mayi.crm.utils.AssertUtil;
import com.mayi.crm.utils.QueryUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author yacong_liu
 * @version 1.0
 * @classname CusDevPlanService
 * @description 客户开发计划业务层
 * @date 2021/1/16
 **/
@Service
public class CusDevPlanService extends BaseService<CusDevPlan, Integer> {

    @Resource
    private CusDevPlanMapper cusDevPlanMapper;

    @Resource
    private SaleChanceMapper saleChanceMapper;

    /**
     * @param [query] 客户开发计划查询对象
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @description 多条件查询客户开发计划
     * @date 2021/1/16 11:16
     **/
    public Map<String, Object> queryCusDevPlanByParams(CusDevPlanQuery query) {
        // 1.分页查询
        PageHelper.startPage(query.getPage(), query.getLimit());
        PageInfo<CusDevPlan> pageInfo = new PageInfo<>(cusDevPlanMapper.selectByParams(query));
        // 2.构建响应数据结构
        return QueryUtil.buildInfo(pageInfo);
    }

    /**
     * @param [cusDevPlan]
     * @return void
     * @description 保存客户开发计划项
     * @date 2021/1/16 12:03
     **/
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveCusDecPlan(CusDevPlan cusDevPlan) {
        // 1.参数校验
        checkParams(cusDevPlan);
        // 2. 设置默认值
        cusDevPlan.setId(null); //防止恶意串改内码
        cusDevPlan.setCreateDate(new Date());
        cusDevPlan.setUpdateDate(new Date());
        cusDevPlan.setIsValid(1); //数据有效状态为有效
        // 3. 执行添加 判断结果
        AssertUtil.isTrue(cusDevPlanMapper.insertSelective(cusDevPlan) < 1, "计划项添加失败！");

    }

    /**
     * @param [cusDevPlan]
     * @return void
     * @description 更新客户计划项
     * @date 2021/1/16 12:03
     **/
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateCusDevPlan(CusDevPlan cusDevPlan) {
        AssertUtil.isTrue((null == cusDevPlan.getId()
                || null == selectByPrimaryKey(cusDevPlan.getId())), "待更新记录不存在！");

        checkParams(cusDevPlan);
        cusDevPlan.setUpdateDate(new Date());
        AssertUtil.isTrue(updateByPrimaryKeySelective(cusDevPlan) < 1, "计划项记录更新失败！");
    }

    /**
     * 删除计划项
     *
     * @param id
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void delCusDevPlan(Integer id) {
        CusDevPlan cusDevPlan = selectByPrimaryKey(id);
        AssertUtil.isTrue((null == id || null == cusDevPlan), "带删除记录不存在！");
        cusDevPlan.setIsValid(0);
        AssertUtil.isTrue(updateByPrimaryKeySelective(cusDevPlan) < 1, "计划项记录删除失败！");
    }

    /**
     * @param [cusDevPlan] 客户开发计划项
     * @return void
     * @description 参数校验
     * 1. 营销机会ID 非空
     * 2. 计划项内码 非空
     * 3. 计划项时间 非空
     * @date 2021/1/16 11:40
     **/
    private void checkParams(CusDevPlan cusDevPlan) {
        AssertUtil.isTrue((null == cusDevPlan.getSaleChanceId()
                        || null == saleChanceMapper.selectByPrimaryKey(cusDevPlan.getSaleChanceId())),
                "请设置营销机会！");
        AssertUtil.isTrue(StringUtils.isBlank(cusDevPlan.getPlanItem()), "请输入计划项内容！");
        AssertUtil.isTrue(null == cusDevPlan.getPlanDate(), "请选择计划项日期！");

    }


}
