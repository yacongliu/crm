package com.mayi.crm.service;

import com.mayi.crm.base.BaseService;
import com.mayi.crm.dao.PermissionMapper;
import com.mayi.crm.model.Permission;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author yacong_liu
 * @version 1.0
 * @classname PermissionService
 * @description 权限业务层
 * @date 2021/1/28
 **/
@Service
public class PermissionService extends BaseService<Permission, Integer> {
    @Resource
    private PermissionMapper permissionMapper;

    /**
     * @param [userId]
     * @return java.util.List<java.lang.String>
     * @description 通过查询用户拥有的角色，角色拥有的资源，得到用户拥有的资源列表 （资源权限码）
     * @date 2021/1/28 09:06
     **/
    public List<String> queryUserHasRoleHasPermissionByUserId(Integer userId) {
        return permissionMapper.queryUserHasRoleHasPermissionByUserId(userId);
    }
}
