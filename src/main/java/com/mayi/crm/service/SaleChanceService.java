package com.mayi.crm.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mayi.crm.base.BaseService;
import com.mayi.crm.dao.SaleChanceMapper;
import com.mayi.crm.enums.DevResultEnum;
import com.mayi.crm.enums.StateStatusEnum;
import com.mayi.crm.model.SaleChance;
import com.mayi.crm.query.SaleChanceQuery;
import com.mayi.crm.utils.AssertUtil;
import com.mayi.crm.utils.PhoneUtil;
import com.mayi.crm.utils.QueryUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author yacong_liu
 * @version 1.0
 * @classname SaleChanceService
 * @description
 * @date 2021/1/14
 **/
@Service
public class SaleChanceService extends BaseService<SaleChance, Integer> {

    @Autowired
    private SaleChanceMapper saleChanceMapper;

    /**
     * @param query 营销机会查询对象
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @description 多条件分页查询
     * @date 2021/1/14 14:49
     **/
    public Map<String, Object> querySaleChanceByParams(SaleChanceQuery query) {

        // 1.分页查询
        PageHelper.startPage(query.getPage(), query.getLimit());
        PageInfo<SaleChance> pageInfo = new PageInfo<>(saleChanceMapper.selectByParams(query));
        // 2.响应统一数据结构
        return QueryUtil.buildInfo(pageInfo);
    }

    /**
     * @param saleChance 营销机会对象
     * @return void
     * @description 新增营销机会
     * 1. 参数校验
     * customerName 非空
     * linkMan 非空
     * linkPhone 非空
     * 2. 设置相关参数默认值
     * state 默认未分配，如果选择了分配人 state为已分配
     * assignTime 分配时间，如果选择了分配人，默认为系统当前时间
     * devResult 开发状态 默认未开发，如果选择了分配人，则为开发中，
     * 0-未开发
     * 1-开发中
     * 2-开发成功
     * 3-开发失败
     * isValid 默认有效数据
     * 1-有效
     * 0-无效
     * createDate updateDate 默认系统当前时间
     * 3. 执行更新 判断结果
     * @date 2021/1/15 08:58
     **/
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveSaleChance(SaleChance saleChance) {
        // 1. 参数校验
        checkParams(saleChance.getCustomerName(), saleChance.getLinkMan(), saleChance.getLinkPhone());
        // 2. 设置默认值
        saleChance.setState(StateStatusEnum.UNSTATE.getType()); //未分配
        saleChance.setDevResult(DevResultEnum.UNDEV.getStatus()); //未开发
        if (StringUtils.isNotBlank(saleChance.getAssignMan())) { //选择了分配人
            saleChance.setState(StateStatusEnum.STATED.getType());
            saleChance.setDevResult(DevResultEnum.DEVING.getStatus());
            saleChance.setAssignTime(new Date());
        }
        saleChance.setIsValid(1);
        saleChance.setCreateDate(new Date());
        saleChance.setUpdateDate(new Date());

        AssertUtil.isTrue(saleChanceMapper.insertSelective(saleChance) < 1, "营销机会数据添加失败！");

    }


    /**
     * @param [saleChance] 营销机会对象
     * @return void
     * @description 更新营销机会
     * @date 2021/1/15 11:00
     **/
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateSaleChance(SaleChance saleChance) {

        // 1. 参数校验
        //  判断记录是否存在
        SaleChance temp = saleChanceMapper.selectByPrimaryKey(saleChance.getId());
        AssertUtil.isTrue(null == temp, "待更新记录不存在！");
        //  基础参数校验
        checkParams(saleChance.getCustomerName(), saleChance.getLinkMan(), saleChance.getLinkPhone());

        // 2. 设置相关参数值
        saleChance.setUpdateDate(new Date());
        if (StringUtils.isBlank(temp.getAssignMan()) && StringUtils.isNotBlank(saleChance.getAssignMan())) {
            //原始未指定分配人 更新时指定
            saleChance.setState(StateStatusEnum.STATED.getType());
            saleChance.setAssignMan(saleChance.getAssignMan());
            saleChance.setAssignTime(new Date());
            saleChance.setDevResult(DevResultEnum.DEVING.getStatus());

        } else if (StringUtils.isNotBlank(temp.getAssignMan()) && StringUtils.isBlank(saleChance.getAssignMan())) {
            // 原始已指定 更新时未指定
            saleChance.setAssignMan("");
            saleChance.setState(StateStatusEnum.UNSTATE.getType());
            saleChance.setAssignTime(null);
            saleChance.setDevResult(DevResultEnum.UNDEV.getStatus());

        }

        // 3. 执行更新 判断结果
        AssertUtil.isTrue(saleChanceMapper.updateByPrimaryKeySelective(saleChance) < 1, "营销机会数据更新失败！");


    }

    /**
     * @param [ids]
     * @return void
     * @description 营销机会删除
     * @date 2021/1/15 15:01
     **/
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteSaleChance(Integer[] ids) {
        // 1. 入参校验
        AssertUtil.isTrue((null == ids || ids.length < 0), "请选择需要删除的数据！");
        // 2. 执行删除 判断结果
        AssertUtil.isTrue(saleChanceMapper.deleteBatch(ids) < 0, "营销机会删除失败！");
    }

    /**
     * @param id        机会ID
     * @param devResult 开发状态
     * @return void
     * @description 更新开发状态
     * @date 2021/1/16 14:57
     **/
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateSaleChanceDevResult(Integer id, Integer devResult) {
        AssertUtil.isTrue(null == id, "待更新记录不存在!");
        SaleChance temp = selectByPrimaryKey(id);
        AssertUtil.isTrue(null == temp, "待更新记录不存在!");
        temp.setDevResult(devResult);
        AssertUtil.isTrue(updateByPrimaryKeySelective(temp) < 1, "机会数据更新失败!");
    }


    /**
     * @param customerName 客户名称
     * @param linkMan      联系人
     * @param linkPhone    联系人电话
     * @return void
     * @description 参数校验
     * @date 2021/1/15 09:05
     **/
    private void checkParams(String customerName, String linkMan, String linkPhone) {

        AssertUtil.isTrue(StringUtils.isBlank(customerName), "请输入客户名称！");
        AssertUtil.isTrue(StringUtils.isBlank(linkMan), "请输入联系人！");
        AssertUtil.isTrue(StringUtils.isBlank(linkPhone), "请输入联系人电话！");
        AssertUtil.isTrue(!PhoneUtil.isMobile(linkPhone), "手机号码格式不正确！");
    }
}
