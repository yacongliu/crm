package com.mayi.crm.utils;

import com.github.pagehelper.PageInfo;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yacong_liu
 * @version 1.0
 * @classname QueryUtil
 * @description 查询工具类，返回统一数据格式
 * @date 2021/1/16
 **/
public class QueryUtil {

    /**
     * @param [pageInfo] PageHelp 分页查询对象
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @description 构建LayUi列表数据格式对象
     * @date 2021/1/16 11:22
     **/
    public static Map<String, Object> buildInfo(PageInfo pageInfo) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("code", 0);
        map.put("msg", "success");
        map.put("count", pageInfo.getTotal());
        map.put("data", pageInfo.getList());
        return map;
    }
}
