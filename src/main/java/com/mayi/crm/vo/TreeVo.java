package com.mayi.crm.vo;

/**
 * @author yacong_liu
 * @version 1.0
 * @classname TreeVo
 * @description zTree 数据结构
 * @date 2021/1/27
 **/
public class TreeVo {

    private Integer id;
    private Integer pId;
    private String name;
    private boolean checked = false; //复选框是否被勾选，默认未勾选

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getpId() {
        return pId;
    }

    public void setpId(Integer pId) {
        this.pId = pId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
