package com.mayi.crm.vo;

/**
 * @author yacong_liu
 * @version 1.0
 * @classname UserVo
 * @description 前台用户对象
 * @date 2021/1/13
 **/
public class UserVo {


    //private Integer userId;
    private String userIdStr;


    private String userName;
    private String trueName;


    /* public Integer getUserId() {
         return userId;
     }

     public void setUserId(Integer userId) {
         this.userId = userId;
     }*/

    public String getUserIdStr() {
        return userIdStr;
    }

    public void setUserIdStr(String userIdStr) {
        this.userIdStr = userIdStr;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }
}
