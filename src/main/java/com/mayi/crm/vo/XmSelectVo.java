package com.mayi.crm.vo;

import java.util.List;

/**
 * @author yacong_liu
 * @version 1.0
 * @classname XmSelectVo
 * @description xm-select下拉框渲染对象
 * @date 2021/1/23
 **/
public class XmSelectVo {
    private String name;
    private String value;
    private boolean selected;
    private boolean disabled;
    private List<XmSelectVo> children;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public List<XmSelectVo> getChildren() {
        return children;
    }

    public void setChildren(List<XmSelectVo> children) {
        this.children = children;
    }
}
