layui.use(['table', 'layer'], function () {
    var layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table;

    /**
     * 渲染表格
     */
    var tableIns = table.render({
        elem: '#saleChanceList', // 表格绑定的ID
        url: ctx + '/sale_chance/list?state=1&flag=1', // 访问数据的地址
        cellMinWidth: 95,
        page: true, // 开启分页
        height: "full-125",
        limits: [10, 15, 20, 25],
        limit: 10,
        toolbar: "#toolbarDemo",
        id: "saleChanceListTable",
        cols: [[
            {type: "checkbox", fixed: "center"},
            {field: "id", title: '编号', fixed: "true", sort: true},
            {field: 'chanceSource', title: '机会来源', align: "center"},
            {field: 'customerName', title: '客户名称', align: 'center'},
            {field: 'cgjl', title: '成功几率', align: 'center'},
            {field: 'overview', title: '概要', align: 'center'},
            {field: 'linkMan', title: '联系人', align: 'center'},
            {field: 'linkPhone', title: '联系电话', align: 'center'},
            {field: 'description', title: '描述', align: 'center'},
            {field: 'createMan', title: '创建人', align: 'center'},
            {field: 'createDate', title: '创建时间', align: 'center', width:150, sort: true},
            {
                field: 'devResult', title: '开发状态',
                align: 'center', templet: function (d) {
                    return formatDevResult(d.devResult);
                }
            },
            {
                title: '操作',
                templet: '#op', fixed: "right", align: "center", minWidth: 120
            }
        ]]
    });

    /**
     * 绑定搜索按钮的点击事件
     */
    $(".search_btn").click(function () {
        table.reload('saleChanceListTable', {
            where: { //设定异步数据接口的额外参数，任意设
                customerName: $("input[name='customerName']").val(), // 客户名
                createMan: $("input[name='createMan']").val(), // 创建人
                devResult: $("#devResult").val() // 开发状态
            }
            ,page: {
                curr: 1 // 重新从第 1 页开始
            }
        }); // 只重载数据
    });

    /**
     * 行监听事件
     */
    table.on("tool(saleChances)", function (obj) {
        var layEvent = obj.event;
        if (layEvent === "info") {
            // 详情
            openCusDevPlanDialog("计划项数据维护", obj.data.id);
        } else if (layEvent === "dev") {
            // 开发
            openCusDevPlanDialog("计划项数据开发", obj.data.id);
        }
    });

    /**
     * 打开开发计划对话框
     * @param title
     * @param id
     */
    function openCusDevPlanDialog(title, sid) {
        layui.layer.open({
            title:title,
            type: 2,
            area:["800px","500px"],
            maxmin: true,
            content:ctx + "/cus_dev_plan/toCusDevPlanDataPage?sid=" + sid
        });
    }


    /**
     * 格式化开发状态
     * 0 - 未开发
     * 1 - 开发中
     * 2 - 开发成功
     * 3 - 开发失败
     * @param val
     * @returns {string}
     */
    function formatDevResult(val) {
        if (val === 0) {
            return "<div style='color: yellow'>未开发</div>";
        } else if (val === 1) {
            return "<div style='color: #00FF00;'>开发中</div>";
        } else if (val === 2) {
            return "<div style='color: #00B83F'>开发成功</div>";
        } else if (val === 3) {
            return "<div style='color: red'>开发失败</div>";
        } else {
            return "<div style='color: #af0000'>未知</div>"
        }
    }


});
