layui.use(['element', 'layer', 'layuimini', 'jquery', 'jquery_cookie'], function () {
    var $ = layui.jquery,
        layer = layui.layer,
        $ = layui.jquery_cookie($);

    // 菜单初始化
    $('#layuiminiHomeTabIframe').html('<iframe width="100%" height="100%" frameborder="0"  src="welcome"></iframe>')
    layuimini.initTab();

    $(".login-out").click(function () {
        //删除cookie
        $.removeCookie("userIdStr", {domain: window.location.hostname, path: ctx});
        $.removeCookie("userName", {domain: window.location.hostname, path: ctx});
        $.removeCookie("trueName", {domain: window.location.hostname, path: ctx});
        //跳转到登录页面
        window.location.href = ctx + "/index";
    });


});