layui.use(['form', 'layer'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;


    /**
     * 提交表单
     */
    form.on("submit(addOrUpdateSaleChance)", function (data) {
        var index = layer.msg("数据提交中，请稍后...", {
            icon: 16,
            time: false,//不关闭
            shade: 0.8 //遮罩层透明度
        });

        var url = ctx + "/sale_chance/save";

        if ($("input[name='id']").val()) { //ID隐藏域不为空则为更新
            url = ctx + "/sale_chance/update";
        }

        $.post(url, data.field, function (result) {
            console.log(result);
            if (result.code === 200) {
                // 1. 提示成功
                layer.msg("添加成功！");
                // 2. 关闭加载层
                layer.close(index);
                // 3. 关闭弹出层
                layer.closeAll("iframe");
                // 4.刷新父页面，重新渲染表格
                parent.location.reload();
            } else {
                layer.msg(result.msg);
            }
        });

        // 阻止表单提交
        return false;
    });

    /**
     * 关闭弹出层
     */
    $('#closeBtn').click(function () {
        // 获取当前弹出层的索引
        var index = parent.layer.getFrameIndex(window.name);
        //关闭弹出层
        parent.layer.close(index);
    });

    /**
     * 加载指派人下拉框
     */

    $.get(ctx + "/user/queryAllSales", function (data) {
        //1. 判断隐藏域指派人的值，区分是否是修改
        var assignMan = $("input[name='assignMan']").val();
        //2. 如果是修改，需要回显指派人
        for (var i = 0; i < data.length; i++) {
            // 如果修改记录的指派人与响应的值中的一直，设置下拉框该项为选中
            if (assignMan == data[i].id) {
                $("#assignMan").append('<option value="' + data[i].id + '" selected>' + data[i].uname + '</option>');

            } else {
                $("#assignMan").append('<option value="' + data[i].id + '">' + data[i].uname + '</option>');
            }
        }

        //3. 重新渲染下拉框
        layui.form.render("select");
    });


});