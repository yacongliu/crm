layui.use(['table', 'layer'], function () {
    var layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table;


    /**
     * 渲染表格
     */
    var tableIns = table.render({
        elem: '#saleChanceList', // 表格绑定的ID
        url: ctx + '/sale_chance/list', // 访问数据的地址
        cellMinWidth: 95,
        page: true, // 开启分页
        height: "full-125",
        limits: [10, 15, 20, 25],
        limit: 10,
        toolbar: "#toolbarDemo",
        id: "saleChanceListTable",
        cols: [[
            {type: "checkbox", fixed: "center"},
            {field: "id", title: '编号', fixed: "true", sort: true},
            {field: 'chanceSource', title: '机会来源', align: "center"},
            {field: 'customerName', title: '客户名称', align: 'center'},
            {field: 'cgjl', title: '成功几率', align: 'center'},
            {field: 'overview', title: '概要', align: 'center'},
            {field: 'linkMan', title: '联系人', align: 'center'},
            {field: 'linkPhone', title: '联系电话', align: 'center'},
            {field: 'description', title: '描述', align: 'center'},
            {field: 'createMan', title: '创建人', align: 'center'},
            {field: 'createDate', title: '创建时间', align: 'center', sort: true},
            {field: 'uname', title: '指派人', align: 'center'},
            {field: 'assignTime', title: '分配时间', align: 'center', sort: true},
            {
                field: 'state', title: '分配状态',
                align: 'center', templet: function (d) {
                    return formatState(d.state);
                }
            },
            {
                field: 'devResult', title: '开发状态',
                align: 'center', templet: function (d) {
                    return formatDevResult(d.devResult);
                }
            },
            {
                title: '操作',
                templet: '#saleChanceListBar', fixed: "right", align: "center", minWidth: 150
            }
        ]]
    });

    $(".search_btn").click(function () {
        table.reload('saleChanceListTable', {
            where: { //设定异步数据接口的额外参数，任意设
                customerName: $("input[name='customerName']").val(), // 客户名
                createMan: $("input[name='createMan']").val(), // 创建人
                state: $("#state").val() // 状态
            }
            , page: {
                curr: 1 // 重新从第 1 页开始
            }
        }); // 只重载数据
    });

    /**
     * 头部工具栏
     */
    table.on('toolbar(saleChances)', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id)
        switch (obj.event) {
            case 'add':
                //打开添加营销机会添加界面
                openAddOrUpdateSaleChanceDialog(null);
                break;
            case 'del':
                deleteSaleChance(checkStatus.data);
                break;
        }
    });

    /**
     * 行工具栏
     */
    table.on('tool(saleChances)', function (obj) {
        var data = obj.data;
        var layevent = obj.event;
        var currentRowId = data.id;
        console.log(currentRowId);

        switch (layevent) {
            case 'edit':
                openAddOrUpdateSaleChanceDialog(currentRowId);
                break
            case 'del':
                deleteOneSaleChance(data.id);
                break

        }
    });

    /**
     * 单个删除营销机会
     * @param id
     */
    function deleteOneSaleChance(id) {
        // 询问是否确认删除
        layer.confirm("确定要删除这条记录吗？", {icon: 3, title: "营销机会数据管理"},
            function (index) {
                // 关闭窗口
                layer.close(index);
                // 发送ajax请求，删除记录
                $.ajax({
                    type: "post",
                    url: ctx + "/sale_chance/del",
                    data: {
                        ids: id
                    },
                    dataType: "json",
                    success: function (result) {
                        if (result.code === 200) {
                            // 加载表格
                            tableIns.reload();
                        } else {
                            layer.msg(result.msg, {icon: 5});
                        }
                    }
                });
            });
    }


    /**
     * 打开营销机会添加或更新界面
     */
    function openAddOrUpdateSaleChanceDialog(saleChanceId) {
        var title = "<h3>营销机会管理-添加机会</h3>";
        var url = ctx + "/sale_chance/addOrUpdateSaleChancePage";

        if (saleChanceId) { //不为空则为修改界面
            title = "<h3>营销机会管理-机会更新</h3>";
            url = url + "?id=" + saleChanceId;
        }

        layui.layer.open({
            title: title,
            type: 2,
            content: url,
            area: ["500px", "550px"],
            maxmin: true
        });
    }

    /**
     *  格式化分配状态
     *   0：未分配
     *   1：已分配
     *   其他：未知
     * @param state
     * @returns {string}
     */
    function formatState(state) {
        if (state === 0) {
            return "<div style='color: royalblue'>未分配</div>";
        } else if (state === 1) {
            return "<div style='color: green'>已分配</div>";
        } else {
            return "<div style='color: red'>未知</div>"
        }
    }

    /**
     * 格式化开发状态
     * 0 - 未开发
     * 1 - 开发中
     * 2 - 开发成功
     * 3 - 开发失败
     * @param val
     * @returns {string}
     */
    function formatDevResult(val) {
        if (val === 0) {
            return "<div style='color: yellowgreen'>未开发</div>";
        } else if (val === 1) {
            return "<div style='color: #00FF00;'>开发中</div>";
        } else if (val === 2) {
            return "<div style='color: #00B83F'>开发成功</div>";
        } else if (val === 3) {
            return "<div style='color: red'>开发失败</div>";
        } else {
            return "<div style='color: #af0000'>未知</div>"
        }
    }

    /**
     * 删除营销机会数据
     * @param data
     */
    function deleteSaleChance(data) {
        if (data.length === 0) {
            layer.msg("请选择要删除的记录！");
            return;
        }

        layer.confirm("确定要删除选中的记录吗？", {btn: ["确定", "取消"]}, function (index) {
            layer.close(index); //关闭确认框
            // 数组 ids=1&ids=2&ids=3
            var ids = "ids=";
            for (var i = 0; i < data.length; i++) {
                if (i < data.length - 1) {
                    ids = ids + data[i].id + "&ids=";
                } else {
                    ids = ids + data[i].id;
                }
            }

            $.ajax({
                type: "post",
                url: ctx + "/sale_chance/del",
                data: ids, // 参数传递的是数组
                dataType: "json",
                success: function (result) {
                    if (result.code === 200) {
                        // 加载表格
                        tableIns.reload();
                    } else {
                        layer.msg(result.msg, {icon: 5});
                    }
                }
            });

        });


    }


});
