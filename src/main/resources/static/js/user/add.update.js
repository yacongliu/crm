layui.use(['form', 'layer', 'xmSelect'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        xmSelect = layui.xmSelect,
        $ = layui.jquery;

    var xmSelect = xmSelect.render({
        el: '#roleIds', //对应div元素
        language: 'zn', //中文
        paging:true, //开启分页
        pageSize:5,// 每页5条
        toolbar: {show: true}, //是否显示工具栏（全选、全清）
        name: 'roleIds', //提交表单的属性名
        data: [] //数据
    });

    /*
    * 搜索渲染 角色
    */
    var userId =  $("input[name='id']").val();
    $.get(ctx + '/role/queryAllRoles?userId='+userId, function (res) {
        xmSelect.update({ //更新多选框数据
            data: res,
            autoRow: true,//自动换行，选中数据过多时，自动换行显示
        });
    });

    /**
     * 添加或更新用户
     */
    form.on("submit(addOrUpdateUser)", function (data) {
        // 弹出loading层
        var index = top.layer.msg('数据提交中，请稍候', {
            icon: 16, time: false,
            shade: 0.8
        });
        var url = ctx + "/user/save";
        if ($("input[name='id']").val()) {
            url = ctx + "/user/update";
        }
        $.post(url, data.field, function (res) {
            if (res.code === 200) {
                setTimeout(function () {
                    // 关闭弹出层（返回值为index的弹出层）
                    top.layer.close(index);
                    top.layer.msg("操作成功！");
                    // 关闭所有ifream层
                    layer.closeAll("iframe");
                    // 刷新父页面
                    parent.location.reload();
                }, 500);
            } else {
                layer.msg(res.msg, {icon: 5});
            }
        });
        return false;
    });

    /**
     * 关闭弹出层
     */
    $("#closeBtn").click(function () {
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层 的索引
        parent.layer.close(index); //再执行关闭
    });
});