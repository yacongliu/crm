layui.use(['form', 'jquery', 'jquery_cookie'], function () {
    var form = layui.form,
        layer = layui.layer,
        $ = layui.jquery;
    $ = layui.jquery_cookie($);

    form.on("submit(saveBtn)", function (res) {
        // 获取表单元素的值
        var fieldData = res.field;

        $.ajax({
            type: "post",
            url: ctx + "/user/updatePassword",
            data: {
                newPassWord: fieldData.new_password,
                oldPassWord: fieldData.old_password,
                confirmPassWord: fieldData.again_password
            },
            dataType: "json",
            success: function (data) {
                if (data.code === 200) {
                    layer.msg("用户密码修改成功，系统将在3秒钟后退出...", function () {
                        // 退出系统后，删除对应的cookie
                        $.removeCookie("userIdStr", {domain: window.location.hostname, path: ctx});
                        $.removeCookie("userName", {domain: window.location.hostname, path: ctx});
                        $.removeCookie("trueName", {domain: window.location.hostname, path: ctx});
                        // 跳转到登录页面 (父窗口跳转)
                        window.parent.location.href = ctx + "/index";
                    });
                } else {
                    layer.msg(data.msg);
                }
            }
        });
        //阻止表单跳转
        return false;
    });

});